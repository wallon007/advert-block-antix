# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-28 18:29+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: block-advert.sh:32
msgid "antiX Advert Blocker"
msgstr ""

#: block-advert.sh:42
#, sh-format
msgid ""
"The <b>$title</b> tool adds stuff to your /etc/hosts file, so \\nthat many "
"advertising servers and websites can't connect to this PC.\\nYou can choose "
"to block ads, malware, pornography, gambling, fakenews and social "
"media\\nBlocking ad servers protects your privacy, saves you bandwidth, "
"greatly \\nimproves web-browsing speed and makes the internet much less "
"annoying in general.\\n\\nDo you want to proceed?"
msgstr ""

#: block-advert.sh:53
msgid ""
"<b> Failed </b> \\n\\n\tantiX advert blocker must be run as root or with "
"sudo "
msgstr ""

#: block-advert.sh:139
msgid "Restoring original /etc/hosts."
msgstr ""

#: block-advert.sh:180
#, sh-format
msgid "Loading  blocklist from $domain"
msgstr ""

#: block-advert.sh:191
msgid ""
"\\n ERROR: \\n No /etc/hosts.ORIGINAL was found, so it can't be restored.\\n "
"Probably you already UNBLOCKED EVERYTHING. \\n You will have to manually "
"edit the file /etc/hosts to remove any unwanted content \\n"
msgstr ""

#: block-advert.sh:213
msgid ""
"Success - your settings have been changed.\\n\\nYour hosts file has been "
"updated.\\nRestart your browser to see the changes."
msgstr ""

#: block-advert.sh:223
msgid ""
"\\n \\n (NOTE1: This application's main window always opens showing the "
"default selection,\\n not what's currently selected. \\n  NOTE2: RESTART "
"YOUR BROWSER TO SEE THE EFFECTS OF ANY CHANGE!)"
msgstr ""

#: block-advert.sh:241
msgid "Choose what to block"
msgstr ""

#: block-advert.sh:242
msgid "UNBLOCK EVERYTHING"
msgstr ""

#: block-advert.sh:243
msgid "Cancel"
msgstr ""

#: block-advert.sh:244
msgid "Block Ad and Malware websites"
msgstr ""

#: block-advert.sh:245
msgid "Block Pornographic websites"
msgstr ""

#: block-advert.sh:246
msgid "Block Fakenews websites"
msgstr ""

#: block-advert.sh:247
msgid "Block Gambling websites"
msgstr ""

#: block-advert.sh:248
msgid "Block Social Media websites"
msgstr ""

#: block-advert.sh:300
msgid "No item selected"
msgstr ""
