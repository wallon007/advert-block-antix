��          \      �       �   K   �           *     J     [  y   z  �  �  �  �  f   
     q  -   �     �      �  �   �    �                                       <b> Failed </b> \n\n	antiX advert blocker must be run as root or with sudo  Choose what to block Loading  blocklist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites can't connect to this PC.\nYou can choose to block ads, malware, pornography, gambling, fakenews and social media\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:10+0000
Last-Translator: Mehmet Akif 9oglu, 2022
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 <b> Başarısız </b> \n\n	antiX reklam engelleyici root olarak veya sudo ile çalıştırılmalıdır Neyin engelleneceğini seç $domain'den engellenenler listesi yükleniyor Seçilmiş öğe yok Özgün /etc/hosts onarılıyor. Başarılı - ayarlarınız değiştirildi.\n\nAna makine dosyalarınız güncellendi.\nDeğişiklikleri görmek için tarayıcıyı yeniden başlatın.  <b>$title</b>aracı, /etc/hosts dosyanıza bir şeyler ekler, böylece \nbirçok reklam sunucusu ve web sitesi bu bilgisayara bağlanamaz.\nReklamları, kötü amaçlı yazılımları, pornografiyi, kumarı, fakenews'i ve sosyal medyayı engellemeyi seç ebilirsiniz\nReklam sunucularını engellemek korumayı korur gizliliğiniz, bant genişliğinden tasarruf etmenizi sağlar, web'de gezinme hızını büyük ölçüde artırır ve interneti genel olarak çok daha az can sıkıcı hale getirir.\n\nDevam etmek istiyor musunuz? 